var mongoose = require('mongoose');
mongoose.connect('mongodb://todo-user:t0d0u532@ds119088.mlab.com:19088/todo-maneh');    

const express = require('express');
const bodyParser = require('body-parser');
// Express application
const app = express();
// MiddleWare
app.use(bodyParser.urlencoded({ extended:true }));
// Set the app to use pug for templating
app.set('view engine', 'pug');
// Importing the router file (search.js etc...)
const indexRoutes = require('./routes/index');
const searchRoutes = require('./routes/search'); //
// Using the searchRoutes
app.use(indexRoutes);
app.use('/search', searchRoutes);

// Development server on port 3000

mongoose.connection.once('open',function(){
    // Development server on port 3000
app.listen(3000, () => {

    console.log("The application is running on port: 3000!")
});
console.log('connection established with the database');

}).on('error', function(error){
    console.log('connection error:', error);
});