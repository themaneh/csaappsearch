const express = require('express');
var CsaModel = require('../data/model');
const router = express.Router();

// Querring mongodb for all users

// {
         //function to help get the the data from the call back function
// }
function findData(CsaModel, results){ 
   
    CsaModel.find({},function(err, data){
        
       if(err) throw err;  
       return results(data);
        // console.log(data);
    });

}
findData(CsaModel, function(data){ 
const dataLength = data.length;
// Serving the search.pug template to the / route
router.get('/', (req, res) => {

    res.render('search');
})

router.post('/', (req, res) => {

    const userInput = req.body.username.toLowerCase(); // username ofr the name='username' set in the form

    // Validation Logics

    // ID and Username validation
    if (/^[0-9]{9}$/.test(userInput) || /^[A-Z]?[a-z]+$/.test(userInput)) {

        // Collection of names in the database
        let nameList = [];
        
        for (let profileIndex = 0; profileIndex < dataLength; profileIndex++) {
            
            
            if (data[profileIndex].ID === userInput) {

                nameList.push(data[profileIndex].name);
                break;
                

            } else if (data[profileIndex].name.toLowerCase().includes(userInput)) {

                nameList.push(data[profileIndex].name);

            }
        

        }

        if (nameList.length) {
            
            // Serving the noresult.pug template
            return res.render('search', { name: nameList });
        } else {
            
            // Returning the nameList on the search.pug templage
            return res.render('noresult', { name: userInput});
        }


        // Wrong input validation
    } else {

        return res.render('invalid');
    }

})
});
// Exporting router

module.exports = router;